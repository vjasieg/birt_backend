var express = require('express');

var cors = require("cors");
var app = express();
var endpoints = require("./endpoints")


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(endpoints)



module.exports = app;