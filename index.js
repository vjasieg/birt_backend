const http = require('http');
const port = 4242
const app = require('./app');
const server = http.createServer(app);

server.listen(port, '0.0.0.0', function() {
    console.log('Server listening on port ' + port);
});